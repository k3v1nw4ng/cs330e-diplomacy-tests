from io import StringIO
from unittest import TestCase, main

from Diplomacy import diplomacy_solve


class TestDiplomacy(TestCase):

    def test_solve_1(self):
        input = StringIO('A Madrid Move London\n B London Hold')
        output = StringIO()
        diplomacy_solve(input, output)
        self.assertEqual(output.getvalue(), 'A [dead]\nB [dead]')

    def test_solve_2(self):
        input = StringIO(
            'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF Dallas Move Paris\nG Houston Move Barcelona')
        output = StringIO()
        diplomacy_solve(input, output)
        self.assertEqual(output.getvalue(
        ), 'A Madrid\nB [dead]\nC [dead]\nD [dead]\nE Austin\nF [dead]\nG Barcelona')

    def test_solve_3(self):
        input = StringIO(
            'A LosAngeles Move SanFrancisco\nB Madrid Move SanFrancisco\nC SanFrancisco Hold\nD Houston Support C\nE Dallas Support C')
        output = StringIO()
        diplomacy_solve(input, output)
        self.assertEqual(
            output.getvalue(), 'A [dead]\nB [dead]\nC SanFrancisco\nD Houston\nE Dallas')


if __name__ == '__main__':
    main()
