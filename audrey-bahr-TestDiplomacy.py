#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

        # -----    
        # solve
        # -----

        def test_solve1(self):

                r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
                w = StringIO()

                diplomacy_solve(r, w)
                self.assertEqual(
                        w.getvalue(), "A [dead]\nB [dead]\n")

        def test_solve2(self):

                r = StringIO("A Austin Move Miami\nB Miami Move Austin")
                w = StringIO()

                diplomacy_solve(r, w)
                self.assertEqual(
                        w.getvalue(), "A Miami\nB Austin\n")

        def test_solve3(self):

                r = StringIO("A Paris Move London\nB London Hold\nC Dublin Support B")
                w = StringIO()

                diplomacy_solve(r, w)
                self.assertEqual(
                        w.getvalue(), "A [dead]\nB London\nC Dublin\n")
    
    
        def test_solve4(self):

                r = StringIO("A Paris Hold\nB Milan Hold")
                w = StringIO()

                diplomacy_solve(r, w)
                self.assertEqual(
                        w.getvalue(), "A Paris\nB Milan\n")
            

        def test_solve5(self):

                r = StringIO("A Paris Hold\nB Austin Move London\nC London Support A")
                w = StringIO()

                diplomacy_solve(r, w)
                self.assertEqual(
                        w.getvalue(), "A Paris\nB [dead]\nC [dead]\n")
            
            
        def test_solve6(self):

                r = StringIO("A Paris Hold\nB Austin Support A")
                w = StringIO()

                diplomacy_solve(r, w)
                self.assertEqual(
                        w.getvalue(), "A Paris\nB Austin\n")
            
            
        def test_solve7(self):

                r = StringIO("A Paris Hold\nB Austin Move Paris\nC Miami Move Austin")
                w = StringIO()

                diplomacy_solve(r, w)
                self.assertEqual(
                        w.getvalue(), "A [dead]\nB [dead]\nC Austin\n")

        def test_solve8(self):

                r = StringIO("A Austin Move Houston\nB Houston Hold\nC Galveston Hold")
                w = StringIO()

                diplomacy_solve(r, w)
                self.assertEqual(
                        w.getvalue(), "A [dead]\nB [dead]\nC Galveston\n")

        def test_solve9(self):

                r = StringIO("A Austin Move Houston\nB Houston Hold\nC Galveston Support A\nD Cypress Move Galveston\nE JerseyVillage Support D")
                w = StringIO()

                diplomacy_solve(r, w)
                self.assertEqual(
                        w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Galveston\nE JerseyVillage\n")
    
    
    
# ----
# main
# ----

if __name__ == "__main__":
    main()

'''#pragma: no cover

'''

