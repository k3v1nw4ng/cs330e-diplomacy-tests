#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # ----
    # read
    # ----

    def test_read_1(self):
        s = StringIO("A Madrid Hold\n")
        l = diplomacy_read(s)
        self.assertEqual(l, [['A', 'Madrid', 'Hold']])

    def test_read_2(self):
        s = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n')
        l = diplomacy_read(s)
        self.assertEqual(l, [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']])

    def test_read_3(self):
        s = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n')
        l = diplomacy_read(s)
        self.assertEqual(l, [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B'], ['E', 'Austin', 'Support', 'A']])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        l = [['A', 'Madrid', 'Hold']]
        ans = diplomacy_eval(l)
        self.assertEqual(ans, [['A', 'Madrid']])

    def test_eval_2(self):
        l = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']]
        ans = diplomacy_eval(l)
        self.assertEqual(ans, [['A', '[dead]'], ['B', 'Madrid'], ['C', 'London']])

    def test_eval_3(self):
        l = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B'], ['E', 'Austin', 'Support', 'A']]
        ans = diplomacy_eval(l)
        self.assertEqual(ans, [['A', '[dead]'], ['B', '[dead]'], ['C', '[dead]'], ['D', 'Paris'], ['E', 'Austin']])

    # ----
    # print
    # ----

    def test_print_1(self):
        w = StringIO()
        l = [['A', 'Madrid']]
        diplomacy_print(w, l)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        l = [['A', '[dead]'], ['B', 'Madrid'], ['C', 'London']]
        diplomacy_print(w, l)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print_3(self):
        w = StringIO()
        l = [['A', '[dead]'], ['B', '[dead]'], ['C', '[dead]'], ['D', 'Paris'], ['E', 'Austin']]
        diplomacy_print(w, l)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Support B\nE Austin Support A\nF Tokyo Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC London\nD Paris\nE Austin\nF Tokyo\n")
            
    def test_solve_5(self):
        r = StringIO("D Dallas Move Quebec\nA Austin Move Galveston\nG Galveston Support Q\nW Washington Support S\nS Seattle Move Quebec\nN Newark Support B\nR Rome Support B\nB Boston Move Quebec\nQ Quebec Hold\nC Copenhagen Move Quebec\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Quebec\nC [dead]\nD [dead]\nG [dead]\nN Newark\nQ [dead]\nR Rome\nS [dead]\nW Washington\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
