#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Amsterdam Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC Amsterdam\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Move Paris\nB Paris Hold\nC Berlin Support A\nD NewYork Move Madrid\nE Paris Move "
                     "Berlin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Madrid\nE [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Move Barcelona")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\nD Barcelona\n")

    def test_solve_4(self):
        r = StringIO("A Atlanta Support B\nB Brooklyn Move Seattle\nC Cleveland Support D\nD Dallas Move Atlanta")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Seattle\nC Cleveland\nD Atlanta\n")

    def test_solve_5(self):
        r = StringIO("H Atlanta Support C\nC Brooklyn Move Seattle\nP Cleveland Support K\nK Dallas Move Atlanta")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "C Seattle\nH [dead]\nK Atlanta\nP Cleveland\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
